# TP2-Linux

- [TP2-Linux](#tp2-linux)
- [I. Un premier serveur web](#i-un-premier-serveur-web)
  - [1. Installation](#1-installation)
  - [2. Avancer vers la maîtrise du service](#2-avancer-vers-la-maîtrise-du-service)
- [II. Une stack web plus avancée](#ii-une-stack-web-plus-avancée)
  - [2. Setup](#2-setup)
    - [A. Serveur Web et NextCloud](#a-serveur-web-et-nextcloud)
    - [B. Base de données](#b-base-de-données)

# I. Un premier serveur web

## 1. Installation

`sudo dnf -y install httpd`

`systemctl start httpd`

`systemctl enable httpd`

`sudo firewall-cmd --add-port:80/tcp`
`sudo firewall-cmd --add-port:80/tcp --permanent`


TEST :
Vérifier que le service est démarré :
`systemctl status http`
```
[kraizix@web ~]$ systemctl status httpd
● httpd.service - The Apache HTTP Server
   Loaded: loaded (/usr/lib/systemd/system/httpd.service; enabled; vendor p>
   Active: active (running) since Wed 2021-09-29 16:30:51 CEST; 9min ago
     Docs: man:httpd.service(8)
 Main PID: 2083 (httpd)
   Status: "Running, listening on: port 80"
    Tasks: 213 (limit: 4934)
   Memory: 25.6M
   CGroup: /system.slice/httpd.service
           ├─2083 /usr/sbin/httpd -DFOREGROUND
           ├─2084 /usr/sbin/httpd -DFOREGROUND
           ├─2085 /usr/sbin/httpd -DFOREGROUND
           ├─2086 /usr/sbin/httpd -DFOREGROUND
           └─2087 /usr/sbin/httpd -DFOREGROUND

```
Vérifier qu'il est configuré pour démarré automatiquement :
`systemctl list-unit-files | grep enabled`

```
[kraizix@web ~]$ systemctl list-unit-files | grep enabled
[...]
httpd.service                              enabled
[...]
```

![](./img/web.png)

## 2. Avancer vers la maîtrise du service
Démarrer le service automatiquement :

`systemctl enable httpd`

Liste des services démarré automatiquement quand la machine s'allume
`systemctl list-unit-files | grep enabled`

```
[kraizix@web ~]$ systemctl list-unit-files | grep enabled
[...]
httpd.service                              enabled
[...]
```

User du service Apache :
```
[...]
User apache
Group apache
[...]
```
Vérification que le service est en cours d'éxécution :
```
[kraizix@web ~]$ ps -ef | grep apache
apache       780     753  0 09:24 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache       781     753  0 09:24 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache       782     753  0 09:24 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache       783     753  0 09:24 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
kraizix     1609    1573  0 09:29 pts/0    00:00:00 grep --color=auto apache
```


On vérifie que l'utilisateur possède les droits de lecture sur le fichier html :
`-rw-r--r--` le dernier `r--`signifie que n'importe quel utilisateur à les droits de lecture sur ce fichier
```
[kraizix@web ~]$ ls -al /usr/share/testpage/
total 12
drwxr-xr-x.  2 root root   24 Sep 29 16:25 .
drwxr-xr-x. 98 root root 4096 Oct  6 16:52 ..
-rw-r--r--.  1 root root 7621 Jun 11 17:23 index.html
```

Création User Apache: 
```
[kraizix@web ~]$ sudo useradd webA -m -d /usr/share/httpd/ -s /sbin/nologin
[sudo] password for kraizix:
useradd: warning: the home directory already exists.
Not copying any file from skel directory into it.
[kraizix@web ~]$ sudo passwd webA
Changing password for user webA.
New password:
BAD PASSWORD: The password is shorter than 8 characters
Retype new password:
passwd: all authentication tokens updated successfully.
```

Vérification :

```
[kraizix@web ~]$ ps -ef | grep webA
webA        1668     753  0 09:39 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
webA        1669     753  0 09:39 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
webA        1670     753  0 09:39 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
webA        1671     753  0 09:39 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
kraizix     1887    1573  0 09:39 pts/0    00:00:00 grep --color=auto webA
```

```
[kraizix@web ~]$ ss -alpn |grep 8080
tcp   LISTEN 0      128                                                        *:8080                   *:*
```

curl :

```
[kraizix@web ~]$ curl localhost:8080
<!doctype html>
<html>
  <head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <title>HTTP Server Test Page powered by: Rocky Linux</title>
    <style type="text/css">
      /*<![CDATA[*/
```
![](./img/web2.png)

```
Listen 8080

Include conf.modules.d/*.conf

User webA
Group webA
```
📁 Fichier [/etc/httpd/conf/httpd.conf](conf/httpd.conf)
# II. Une stack web plus avancée

## 2. Setup

### A. Serveur Web et NextCloud

Installation des packages :

`sudo dnf install epel-release`
`sudo dnf install https://rpms.remirepo.net/enterprise/remi-release-8.rpm`
`sudo dnf module enable php:remi-8.0`
`sudo dnf install httpd mariadb-server vim wget zip unzip libxml2 openssl php74-php php74-php-ctype php74-php-curl php74-php-gd php74-php-iconv php74-php-json php74-php-libxml php74-php-mbstring php74-php-openssl php74-php-posix php74-php-session php74-php-xml php74-php-zip php74-php-zlib php74-php-pdo php74-php-mysqlnd php74-php-intl php74-php-bcmath php74-php-gmp`

Configuration Nextcloud :
Il faut d'abord créer le dossier qui contiendra les fichiers de config dans /etc/httpd/ : `sudo mkdir -p /etc/httpd/sites-available/`
Création du fichier de configuration de nextcloud :
`sudo vi /etc/httpd/sites-available/linux.tp2.web`
Création du lien vers le fichier de configuration :
`sudo mkdir -p /etc/httpd/sites-enabled/`
`sudo ln -s /etc/httpd/sites-available/linux.tp2.web /etc/httpd/sites-enabled/`

Création du DocumentRoot:
`sudo mkdir -p /var/www/sub-domains/linux.tp2.web/html`

Configuration de PHP :
Il faut insérer le fuseau horaire utilisé par la machine dans le fichier de configuration à la ligne `date.timezone=`
> Pour trouver le fuseau horaire  `timedatectl | grep "Time zone"`
`sudo vi /etc/opt/remi/php74/php.ini`
résultat :
```
cat /etc/opt/remi/php74/php.ini | grep date.timezone
; http://php.net/date.timezone
date.timezone = "Europe/Paris"
```
Installation de nextcloud : 
récupération des fichiers de nextcloud
`wget https://download.nextcloud.com/server/releases/nextcloud-21.0.1.zip`

`unzip nextcloud-21.0.1.zip`

déplacement des fichiers : 
`cp -Rf * /var/www/sub-domains/linux.tp2.web/html`

On donne les droits à l'utilisateur apache :
`chown -Rf apache.apache /var/www/sub-domains/linux.tp2.web/html`

modification du fichier de conf d'apache il faut ajouter la ligne `IncludeOptional sites-enabled/*`afin qu'apache lise les fichiers de conf : 
```
cat /etc/httpd/conf/httpd.conf
[...]
IncludeOptional conf.d/*.conf
IncludeOptional sites-enabled/*

```
On redémarre ensuite le service apache pour prendre en compte les modifications :
`sudo systemctl restart httpd`
![](./img/nextcloud.PNG)

📁 Fichier [/etc/httpd/conf/httpd.conf](conf/httpd2.conf)
📁 Fichier [/etc/httpd/sites-available/web.tp2.linux](conf/linux.tp2.web)



### B. Base de données

Installation des packages:
`sudo dnf install mariadb-server`
Mise en service du serveur mariadb : 
`systemctl enable mariadb`
`systemctl start mariadb`

Configuration de mariaDB :
`mysql_secure_installation`
```
[kraizix@db ~]$ mysql_secure_installation

NOTE: RUNNING ALL PARTS OF THIS SCRIPT IS RECOMMENDED FOR ALL MariaDB
      SERVERS IN PRODUCTION USE!  PLEASE READ EACH STEP CAREFULLY!

In order to log into MariaDB to secure it, we'll need the current
password for the root user.  If you've just installed MariaDB, and
you haven't set the root password yet, the password will be blank,
so you should just press enter here.

Enter current password for root (enter for none):
```
on appuie sur entrer car aucun mot de passe pour root n'est configuré

On choisis de configurer un mot de passe pour root en écrivant 'y'
```
OK, successfully used password, moving on...

Setting the root password ensures that nobody can log into the MariaDB
root user without the proper authorisation.

Set the root password? [Y/n]

```
puis on insère le mot de passe
On continue de presser la touche entrée pour les étapes suivantes afin de les valider

Configuration de la base de données:
```
[kraizix@db ~]$ sudo mysql -u root -p
[sudo] password for kraizix:
Sorry, try again.
[sudo] password for kraizix:
Enter password:
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 17
Server version: 10.3.28-MariaDB MariaDB Server

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB [(none)]> CREATE USER 'nextcloud'@'10.102.1.11' IDENTIFIED BY 'meow';
Query OK, 0 rows affected (0.000 sec)

MariaDB [(none)]> CREATE DATABASE IF NOT EXISTS nextcloud CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
Query OK, 1 row affected (0.001 sec)

MariaDB [(none)]> GRANT ALL PRIVILEGES ON nextcloud.* TO 'nextcloud'@'10.102.1.11';
Query OK, 0 rows affected (0.000 sec)

MariaDB [(none)]> FLUSH PRIVILEGES;
Query OK, 0 rows affected (0.000 sec)

```
On ajoute le port 330- aux ports autorisés par le firewall :
`sudo firewall-cmd --add-port:3306/tcp --permanent`
`sudo firewall-cmd --add-port:3306/tcp`

Connexion à la base de données depuis le terminal de la machine web : `mysql -u nextcloud -h 10.102.1.12 -p`
```
[kraizix@web ~]$ mysql -u nextcloud -h 10.102.1.12 -p
Enter password:
Welcome to the MySQL monitor.  Commands end with ; or \g.
Your MySQL connection id is 112
Server version: 5.5.5-10.3.28-MariaDB MariaDB Server

Copyright (c) 2000, 2021, Oracle and/or its affiliates.

Oracle is a registered trademark of Oracle Corporation and/or its
affiliates. Other names may be trademarks of their respective
owners.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.
```

Lister tous les utilisateurs : `SELECT User FROM mysql.user`
```
MariaDB [(none)]> SELECT User FROM mysql.user;
+-----------+
| User      |
+-----------+
| nextcloud |
| root      |
| root      |
| root      |
+-----------+
4 rows in set (0.000 sec)
```

Modification du fichier hosts sur windows situé dans 'C:\Windows\System32\drivers\etc' et ajout de la ligne `10.102.1.11 web.tp2.linux`

Connexion à la database suite  à l'installation
```
[kraizix@web ~]$ mysql -u nextcloud -h 10.102.1.12 -p
Enter password:
[...]
mysql> SELECT COUNT(*) FROM information_schema.tables WHERE table_schema = 'nextcloud'
    -> ;
+----------+
| COUNT(*) |
+----------+
|       77 |
+----------+
1 row in set (0.01 sec)
```

| Machine         | IP            | Service                 | Port ouvert | IP autorisées |
|-----------------|---------------|-------------------------|-------------|---------------|
| `web.tp2.linux` | `10.102.1.11` | Serveur Web             | 80          | toutes       |
| `db.tp2.linux`  | `10.102.1.12` | Serveur Base de Données | 3306           | 10.102.1.11             |
