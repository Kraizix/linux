# TP1 Linux

- [TP1 Linux](#tp1-linux)
  - [Préparation de la machine :](#préparation-de-la-machine-)
  - [Utilisateurs :](#utilisateurs-)
    - [Création et configuration :](#création-et-configuration-)
    - [SSH :](#ssh-)
  - [Partitionnement :](#partitionnement-)
  - [Gestions des services :](#gestions-des-services-)
    - [Interaction avec un service existant :](#interaction-avec-un-service-existant-)
    - [Creation de services :](#creation-de-services-)
      - [Unité simpliste :](#unité-simpliste-)
      - [Modification de l'unité :](#modification-de-lunité-)
## Préparation de la machine :

* Vérification réseau internet :
Carte réseaux : `ip a`


```
[kraizix@node1 ~]$ ip a
[...]
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:d0:41:09 brd ff:ff:ff:ff:ff:ff
    inet 10.0.2.15/24 brd 10.0.2.255 scope global dynamic noprefixroute enp0s3
       valid_lft 86241sec preferred_lft 86241sec
    inet6 fe80::a00:27ff:fed0:4109/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
[...]
```
Route par défaut : `ip r s`
```
[kraizix@node1 ~]$ ip r s
default via 10.0.2.2 dev enp0s3 proto dhcp metric 100
10.0.2.0/24 dev enp0s3 proto kernel scope link src 10.0.2.15 metric 100
10.101.1.0/24 dev enp0s8 proto kernel scope link src 10.101.1.11 metric 101
```
On ping le serveur de dns pour prouver que l'on possède un accès internet : `ping 8.8.8.8`
```
[kraizix@node1 ~]$ ping 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=114 time=18.5 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=114 time=18.3 ms
^C
--- 8.8.8.8 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1002ms
rtt min/avg/max/mdev = 18.333/18.393/18.453/0.060 ms
```

* Réseau Local :

Mise en place des carte réseaux:
> commande : `sudo nano /etc/sysconfig/network-scripts/ifcfg-enp0s8`
```
  GNU nano 2.9.8                                           /etc/sysconfig/network-scripts/ifcfg-enp0s8                                                      
TYPE=Ethernet
PROXY_METHOD=none
BROWSER_ONLY=no
BOOTPROTO=static
DEFROUTE=yes
IPV4_FAILURE_FATAL=no
IPV6INIT=yes
IPV6_AUTOCONF=yes
IPV6_DEFROUTE=yes
IPV6_FAILURE_FATAL=no
NAME=enp0s8
UUID=c748581a-f1f8-4ecb-b0a2-9cbc64464784
DEVICE=enp0s8
ONBOOT=yes
IPADDR=10.101.1.11
NETMASK=255.255.255.0
```
```
  GNU nano 2.9.8                                           /etc/sysconfig/network-scripts/ifcfg-enp0s8                                                      
TYPE=Ethernet
PROXY_METHOD=none
BROWSER_ONLY=no
BOOTPROTO=static
DEFROUTE=yes
IPV4_FAILURE_FATAL=no
IPV6INIT=yes
IPV6_AUTOCONF=yes
IPV6_DEFROUTE=yes
IPV6_FAILURE_FATAL=no
NAME=enp0s8
UUID=c748581a-f1f8-4ecb-b0a2-9cbc64464784
DEVICE=enp0s8
ONBOOT=yes
IPADDR=10.101.1.12
NETMASK=255.255.255.0
```
Machine 1:
```
[kraizix@node1 ~]$ ip a
[...]
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:68:ef:08 brd ff:ff:ff:ff:ff:ff
    inet 10.101.1.11/24 brd 10.101.1.255 scope global noprefixroute enp0s8
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fe68:ef08/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
```
Machine 2:

```
[kraizix@node2 ~]$ ip a
[...]
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:bd:b5:4c brd ff:ff:ff:ff:ff:ff
    inet 10.101.1.12/24 brd 10.101.1.255 scope global noprefixroute enp0s8
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:febd:b54c/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
       ```

```
On vérifie que les 2 machines sont sur le même réseau en faisant un ping
```
[kraizix@node1 ~]$ ping 10.101.1.12
PING 10.101.1.12 (10.101.1.12) 56(84) bytes of data.
64 bytes from 10.101.1.12: icmp_seq=1 ttl=64 time=1.08 ms
64 bytes from 10.101.1.12: icmp_seq=2 ttl=64 time=0.627 ms
64 bytes from 10.101.1.12: icmp_seq=3 ttl=64 time=0.632 ms
^C
--- 10.101.1.12 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2061ms
rtt min/avg/max/mdev = 0.627/0.778/1.075/0.210 ms
```

* Hostname :

`sudo hostname <name>`

`sudo nano /etc/hostname`

On vérifie l'hostname avec la commande : `hostname`

```
[kraizix@node1 ~]$ hostname
node1.tp1.b2
```

```
[kraizix@node2 ~]$ hostname
node2.tp1.b2
```

* DNS :

Configuration serveur DNS en modifiant /etc/resolv.conf
`sudo nano /etc/resolv.conf`
On écrit les lignes suivantes  dans le fichier afin de changer le serveur dns
```
nameserver 1.1.1.1
```

On installe la commande dig en faisant : `dnf provides dig`

```
[kraizix@node1 ~]$ dig ynov.com

; <<>> DiG 9.11.26-RedHat-9.11.26-4.el8_4 <<>> ynov.com
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 10855
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1232
;; QUESTION SECTION:
;ynov.com.                      IN      A

;; ANSWER SECTION:
ynov.com.               9659    IN      A       92.243.16.143

;; Query time: 18 msec
;; SERVER: 1.1.1.1#53(1.1.1.1)
;; WHEN: Wed Sep 22 16:51:12 CEST 2021
;; MSG SIZE  rcvd: 53
```
Ligne contenant L'IP correspondant au nom de domaine : `ynov.com.               9659    IN      A       92.243.16.143`
Ligne contenant l'IP du serveur ayant répondu : `SERVER: 1.1.1.1#53(1.1.1.1)`

* Host :

On peut ajouter des hosts dans le fichier /etc/hosts

`sudo nano /etc/hosts`

```
127.0.0.1   localhost localhost.localdomain localhost4 localhost4.localdomain4
::1         localhost localhost.localdomain localhost6 localhost6.localdomain6
10.101.1.12  node2 node2.tp1.b2
```
```
127.0.0.1   localhost localhost.localdomain localhost4 localhost4.localdomain4
::1         localhost localhost.localdomain localhost6 localhost6.localdomain6
10.101.1.11 node1 node1.tp1.b2
```

* Firewall :

commande `sudo firewall-cmd --list-all` 

```
[kraizix@node1 ~]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services: cockpit dhcpv6-client ssh
  ports:
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
  ```

## Utilisateurs :

### Création et configuration :

`sudo useradd -m -s /bin/sh toto`


Groupes : 
`sudo groupadd admins`

`visudo`

```%wheel ALL=(ALL) ALL```

Ajout de l'utilisateur au groupe admins :
`usermod -aG admins toto`

### SSH :

`ssh-keygen -t ed25519`
Transfert de la clé :
`type C:\Users\kevin\.ssh\id_ed25519.pub | ssh toto@10.101.1.11 "cat >> .ssh/authorized_keys"`

```
PS C:\Users\kevin> type C:\Users\kevin\.ssh\id_ed25519.pub | ssh toto@10.101.1.11 "cat >> .ssh/authorized_keys"
toto@10.101.1.11's password:
PS C:\Users\kevin> ssh toto@10.101.1.11
Activate the web console with: systemctl enable --now cockpit.socket

Last login: Wed Sep 22 20:27:37 2021 from 10.101.1.1
[toto@node1 ~]$
```

## Partitionnement :

```
[toto@node1 ~]$ lsblk
NAME        MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
sda           8:0    0    8G  0 disk
├─sda1        8:1    0    1G  0 part /boot
└─sda2        8:2    0    7G  0 part
  ├─rl-root 253:0    0  6.2G  0 lvm  /
  └─rl-swap 253:1    0  820M  0 lvm  [SWAP]
sdb           8:16   0    3G  0 disk
sdc           8:32   0    3G  0 disk
sr0          11:0    1 1024M  0 rom
```

Ajout des disques sur LVM:

```
[toto@node1 ~]$ sudo pvcreate /dev/sdb
[sudo] password for toto:
  Physical volume "/dev/sdb" successfully created.
[toto@node1 ~]$ sudo pvcreate /dev/sdc
  Physical volume "/dev/sdc" successfully created.
```

vérification :
```
[toto@node1 ~]$ sudo pvs
  PV         VG Fmt  Attr PSize  PFree
  /dev/sda2  rl lvm2 a--  <7.00g    0
  /dev/sdb      lvm2 ---   3.00g 3.00g
  /dev/sdc      lvm2 ---   3.00g 3.00g
```

Création du volume group
```
[toto@node1 ~]$ sudo vgcreate data /dev/sdb
  Volume group "data" successfully created
[toto@node1 ~]$ sudo vgs
  VG   #PV #LV #SN Attr   VSize  VFree
  data   1   0   0 wz--n- <3.00g <3.00g
  rl     1   2   0 wz--n- <7.00g     0
```
Ajout du deuxième disque au volume group
```
[toto@node1 ~]$ sudo vgextend data /dev/sdc
  Volume group "data" successfully extended
[toto@node1 ~]$ sudo vgs
  VG   #PV #LV #SN Attr   VSize  VFree
  data   2   0   0 wz--n-  5.99g 5.99g
  rl     1   2   0 wz--n- <7.00g    0
```
Création des 3 logical volume de 1Go
```
[toto@node1 ~]$ sudo lvcreate -L 1G data -n lv1
  Logical volume "lv1" created.
[toto@node1 ~]$ sudo lvcreate -L 1G data -n lv2
  Logical volume "lv2" created.
[toto@node1 ~]$ sudo lvcreate -L 1G data -n lv3
  Logical volume "lv3" created.
[toto@node1 ~]$ sudo lvs
  LV   VG   Attr       LSize   Pool Origin Data%  Meta%  Move Log Cpy%Sync Convert
  lv1  data -wi-a-----   1.00g

  lv2  data -wi-a-----   1.00g

  lv3  data -wi-a-----   1.00g

  root rl   -wi-ao----  <6.20g

  swap rl   -wi-ao---- 820.00m
```

Formatage :
```
[toto@node1 ~]$ sudo mkfs -t ext4 /dev/data/lv2
mke2fs 1.45.6 (20-Mar-2020)
Creating filesystem with 262144 4k blocks and 65536 inodes
Filesystem UUID: 895f0e81-b250-47f9-9416-9b4505387043
Superblock backups stored on blocks:
        32768, 98304, 163840, 229376

Allocating group tables: done
Writing inode tables: done
Creating journal (8192 blocks): done
Writing superblocks and filesystem accounting information: done

[toto@node1 ~]$ sudo mkfs -t ext4 /dev/data/lv3
mke2fs 1.45.6 (20-Mar-2020)
Creating filesystem with 262144 4k blocks and 65536 inodes
Filesystem UUID: 636b14c2-71b5-4fb8-9152-466ec98b95a9
Superblock backups stored on blocks:
        32768, 98304, 163840, 229376

Allocating group tables: done
Writing inode tables: done
Creating journal (8192 blocks): done
Writing superblocks and filesystem accounting information: done
```

Montage des partitions :

```
[toto@node1 ~]$ sudo mount /dev/data/lv1 /mnt/part1
[toto@node1 ~]$ sudo mount /dev/data/lv2 /mnt/part2
[toto@node1 ~]$ sudo mount /dev/data/lv3 /mnt/part3
[toto@node1 ~]$ df -h
Filesystem            Size  Used Avail Use% Mounted on
devtmpfs              891M     0  891M   0% /dev
tmpfs                 909M     0  909M   0% /dev/shm
tmpfs                 909M  8.5M  901M   1% /run
tmpfs                 909M     0  909M   0% /sys/fs/cgroup
/dev/mapper/rl-root   6.2G  2.1G  4.2G  33% /
/dev/sda1            1014M  241M  774M  24% /boot
tmpfs                 182M     0  182M   0% /run/user/1001
/dev/mapper/data-lv1  976M  2.6M  907M   1% /mnt/part1
/dev/mapper/data-lv2  976M  2.6M  907M   1% /mnt/part2
/dev/mapper/data-lv3  976M  2.6M  907M   1% /mnt/part3
```
Montage automatique des partitions :

`sudo nano /etc/fstab`
```
/dev/data/lv1           /mnt/part1              ext4    defaults        0 0
/dev/data/lv2           /mnt/part2              ext4    defaults        0 0
/dev/data/lv3           /mnt/part3              ext4    defaults        0 0
```

## Gestions des services :

### Interaction avec un service existant :

```
[toto@node1 ~]$ systemctl is-active firewalld
active
[toto@node1 ~]$ systemctl is-enabled firewalld
enabled
```
### Creation de services :

#### Unité simpliste :

Mise en place du service :
```
[toto@node1 system]$ sudo systemctl start web
[toto@node1 system]$ sudo systemctl enable web
Created symlink /etc/systemd/system/multi-user.target.wants/web.service → /etc/systemd/system/web.service.
```

```
PS C:\Users\kevin> curl 10.101.1.11:8888
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Directory listing for /</title>
</head>
<body>
<h1>Directory listing for /</h1>
<hr>
<ul>
<li><a href="bin/">bin@</a></li>
<li><a href="boot/">boot/</a></li>
<li><a href="dev/">dev/</a></li>
<li><a href="etc/">etc/</a></li>
<li><a href="home/">home/</a></li>
<li><a href="lib/">lib@</a></li>
<li><a href="lib64/">lib64@</a></li>
<li><a href="media/">media/</a></li>
<li><a href="mnt/">mnt/</a></li>
<li><a href="opt/">opt/</a></li>
<li><a href="proc/">proc/</a></li>
<li><a href="root/">root/</a></li>
<li><a href="run/">run/</a></li>
<li><a href="sbin/">sbin@</a></li>
<li><a href="srv/">srv/</a></li>
<li><a href="sys/">sys/</a></li>
<li><a href="tmp/">tmp/</a></li>
<li><a href="usr/">usr/</a></li>
<li><a href="var/">var/</a></li>
</ul>
<hr>
</body>
</html>

```

#### Modification de l'unité :

Création de l'utilisateur `web`:



Modification du service:

Commande : `sudo nano /etc/systemd/system/web.service`
```
[Unit]
Description=Very simple web service

[Service]
ExecStart=/bin/python3 -m http.server 8888

User=web
WorkingDirectory=/srv/web
[Install]
WantedBy=multi-user.target
```


Création du fichier

```
[web@node1 web]$ touch file.txt
[web@node1 web]$ ls
file.txt
[web@node1 web]$ ls -l
total 0
-rw-rw-r--. 1 web web 0 Sep 26 20:04 file.txt
[web@node1 web]$ ls -l /srv
total 0
drwxr-xr-x. 2 web web 22 Sep 26 20:04 web
```


Vérification :

commande : `curl 10.101.1.11:8888/srv/web`
```
PS C:\Users\kevin> curl 10.101.1.11:8888
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Directory listing for /</title>
</head>
<body>
<h1>Directory listing for /</h1>
<hr>
<ul>
<li><a href="bin/">bin@</a></li>
<li><a href="boot/">boot/</a></li>
<li><a href="dev/">dev/</a></li>
<li><a href="etc/">etc/</a></li>
<li><a href="home/">home/</a></li>
<li><a href="lib/">lib@</a></li>
<li><a href="lib64/">lib64@</a></li>
<li><a href="media/">media/</a></li>
<li><a href="mnt/">mnt/</a></li>
<li><a href="opt/">opt/</a></li>
<li><a href="proc/">proc/</a></li>
<li><a href="root/">root/</a></li>
<li><a href="run/">run/</a></li>
<li><a href="sbin/">sbin@</a></li>
<li><a href="srv/">srv/</a></li>
<li><a href="sys/">sys/</a></li>
<li><a href="tmp/">tmp/</a></li>
<li><a href="usr/">usr/</a></li>
<li><a href="var/">var/</a></li>
</ul>
<hr>
</body>
</html>
```
