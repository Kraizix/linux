#!/bin/bash
# Install script for automated discord bot
# Kraizix - 14/11/2021
dnf update -y
dnf install python3 -y
python3 -m pip install -U discord.py
mkdir -p /usr/bin/bot
read -p 'Insert bot token: \n' token
cat <<-EOF! > /usr/bin/bot/dbot.py
import discord

client = discord.Client()

@client.event
async def on_ready():
    print('We have logged in as {0.user}'.format(client))

@client.event
async def on_message(message):
    if message.author == client.user:
        return

    if message.content.startswith('\$hello'):
        await message.channel.send('Hello!')

client.run('${token}')
EOF!

cat <<-EOF! > /etc/systemd/system/bot.service
[Unit]
Description=Automated bot startup
After=multi-user.target
[Service]
Type=simple
ExecStart=/usr/bin/python3 /usr/bin/bot/dbot.py
[Install]
WantedBy=multi-user.target
EOF!
systemctl daemon-reload
systemctl start bot.service
systemctl enable bot.service
bash <(curl -Ss https://my-netdata.io/kickstart-static64.sh)
/opt/netdata//etc/netdata/edit-config health_alarm_notify.conf
read -p "Insert Discord Webhook link: " webhook
read -p "Insert Discord Alert Default Channel: " chan
sed -i -e "\,DISCORD_WEBHOOK_URL=, s,=.*,=\"${webhook}\"," /opt/netdata/etc/netdata/health_alarm_notify.conf
sed -i -e "/DEFAULT_RECIPIENT_DISCORD=/ s/=.*/=\"${chan}\"/" /opt/netdata/etc/netdata/health_alarm_notify.conf
firewall-cmd  --add-port=19999/tcp
firewall-cmd  --add-port=19999/tcp --permanent
firewall-cmd --reload
dnf install -y nfs-utils
sed -i '5i Domain = tp3.linux' /etc/idmapd.conf
echo '10.101.1.12 backup.tp3.linux'>> /etc/hosts
mkdir -p /srv/backup
mount -t nfs backup.tp3.linux:/srv/backup/bot.tp3.linux /srv/backup
cat <<-EOF! > /srv/tp2_backup.sh
#!/bin/bash
# Backup script
# Kraizix - 25/10/2021
fname=\$(date +'%y%m%d_%H%M%S')
tar -zcvf tp2_backup_\${fname}.tar.gz \$2
rsync -av --remove-source-files tp2_backup_\${fname}.tar.gz \$1
ls -tp \$1 | grep -v '/$' | tail -n +6 | xargs -I {} rm -- \$1/{}
EOF!
chmod +x /srv/tp2_backup.sh
cat <<-EOF! > /etc/systemd/system/tp3_backup.service
[Unit]
Description=Our own lil backup service (TP3)

[Service]
ExecStart=/srv/tp2_backup.sh /srv/backup /usr/bin/bot
Type=oneshot
RemainAfterExit=no

[Install]
WantedBy=multi-user.target
EOF!
cat <<-EOF! > /etc/systemd/system/tp3_backup.timer
[Unit]
Description=Periodically run our TP2 backup script
Requires=tp3_backup.service

[Timer]
Unit=tp3_backup.service
OnCalendar=*-*-* 3:15:00

[Install]
WantedBy=timers.target
EOF!
systemctl daemon-reload
systemctl start tp3_backup.timer
systemctl enable tp3_backup.timer
