# TP3 Bot Discord

- [TP3 Bot Discord](#tp3-bot-discord)
- [Installation :](#installation-)
- [I. Backup](#i-backup)
  - [Installation de nfs](#installation-de-nfs)
  - [Installation du monitoring](#installation-du-monitoring)
- [II. Installation du bot sur une machine](#ii-installation-du-bot-sur-une-machine)
  - [Création du bot depuis discord](#création-du-bot-depuis-discord)
  - [Implémentation d'un script pour permettre au bot d'interragir](#implémentation-dun-script-pour-permettre-au-bot-dinterragir)
  - [Création d'un service pour allumer le programme au démarrage](#création-dun-service-pour-allumer-le-programme-au-démarrage)
  - [Installation du monitoring](#installation-du-monitoring-1)
  - [Config de la backup](#config-de-la-backup)
- [III. Bot dashboard](#iii-bot-dashboard)
  - [Installation du monitoring](#installation-du-monitoring-2)
  - [Config de la backup](#config-de-la-backup-1)
- [IV. Récap](#iv-récap)
# Installation :

Suivez le README ou utilisez les scripts .sh fournis
il faut commencer par le `backup.sh` (lors de l'exécution du script une fenêtre vi lors de l'installation de netdata sur chaque script va apparaitre il faut la quitter en faisant ':q' et appuyer sur entrée)

Pour lancer un script il faut donner les droits d'exécution au script `chmod +x script.sh` puis utiliser la commande `sudo ./script.sh`
# I. Backup
## Installation de nfs
Installation des paquets
`sudo dnf -y install nfs-utils`

On modifie  le fichier /etc/idmapd.conf en décommentant la ligne Domain = puis en la modifiant comme ceci : `Domain = tp3.linux` avec
`sudo vim /etc/idmapd.conf` 

On modifie  le fichier /etc/exports en ajoutant la ligne suivante: `/srv/backup 10.101.1.0/24(rw,no_root_squash)` avec 
`sudo vim /etc/exports`

On créé ensuite le dossier de backup

`sudo mkdir -p /srv/backup/web.tp3.linux`

`sudo mkdir -p /srv/backup/bot.tp3.linux`

Setup du firewall :

on config la zone de partage de fichier nfs
```
firewall-cmd --add-port=2049/tcp --permanent
firewall-cmd --add-port=2049/tcp
firewall-cmd --reload
```

## Installation du monitoring

Pour le monitoring de la machine on utilise Netdata

```bash
sudo -
bash <(curl -Ss https://my-netdata.io/kickstart-static64.sh)
exit
```

On ajoute le port de netdata puis on reload
```
firewall-cmd  --add-port=19999/tcp
firewall-cmd  --add-port=19999/tcp --permanent
firewall-cmd --reload
```

Optionnel - Ajout de l'alerting via discord :
`sudo /opt/netdata//etc/netdata/edit-config health_alarm_notify.conf`
 modifier la ligne DISCORD_WEBHOOK_URL="" et ajoutez l'url de votre webhook puis ajouter le channel ici DEFAULT_RECIPIENT_DISCORD=""

 Test de l'alerting :
 ```
 [kraizix@bot ~]$ sudo su -s /bin/bash netdata
bash-4.4$ export NETDATA_ALARM_NOTIFY_DEBUG=1
bash-4.4$ /opt/netdata/usr/libexec/netdata/plugins.d/alarm-notify.sh test 
 ```
# II. Installation du bot sur une machine


## Création du bot depuis discord

- Connectez-vous/Inscrivez-vous sur le site de Discord

- Rendez vous sur cette page https://discord.com/developers/applications

- Cliquez sur 'New Application' et donnez un nom a votre applications
- Ensuite allez dans l'onglet bot et créez en un
- Enfin il vous suffit de récupérer le token de votre bot pour la suite de l'installation

## Implémentation d'un script pour permettre au bot d'interragir

On effectue dans un premier temps une mise à jour de tous les paquets de la machine

`sudo dnf update -y`

On installe ensuite python qui sera nécessaire afin d'exécuter le script

`sudo dnf install python3 -y`

Installation de la librairie discord.py :
`sudo python3 -m pip install -U discord.py`

Import du premier script du bot (il faut insérer le token du bot que vous avez créer) :

```bash
cat <<-EOF! > /usr/bin/bot/dbot.py
import discord

client = discord.Client()

@client.event
async def on_ready():
    print('We have logged in as {0.user}'.format(client))

@client.event
async def on_message(message):
    if message.author == client.user:
        return

    if message.content.startswith('\$hello'):
        await message.channel.send('Hello!')

client.run('${token}')
EOF!

```
Pour lancer le script manuellement il suffit d'utiliser la commande suivante : `python3 /usr/bin/botdbot.py`


## Création d'un service pour allumer le programme au démarrage

On crée le service : `sudo vim /etc/systemd/system/bot.service` et on insère les lignes suivantes :

```
[Unit]
Description=Automated bot startup
After=multi-user.target
[Service]
Type=simple
ExecStart=/usr/bin/python3 /usr/bin/bot/dbot.py
[Install]
WantedBy=multi-user.target
```
Démarrage du service
```
sudo systemctl daemon-reload
sudo systemctl start bot.service
sudo systemctl enable bot.service
```


![](bot.png)
## Installation du monitoring

Pour le monitoring de la machine on utilise Netdata

```bash
sudo -
bash <(curl -Ss https://my-netdata.io/kickstart-static64.sh)
exit
```
On ajoute le port de netdata puis on reload
```
firewall-cmd  --add-port=19999/tcp
firewall-cmd  --add-port=19999/tcp --permanent
firewall-cmd --reload
```

Optionnel - Ajout de l'alerting via discord :
`sudo /opt/netdata//etc/netdata/edit-config health_alarm_notify.conf`

 modifier la ligne DISCORD_WEBHOOK_URL="" et ajoutez l'url de votre webhook puis ajouter le channel ici DEFAULT_RECIPIENT_DISCORD=""

 Test de l'alerting :
 ```
 [kraizix@bot ~]$ sudo su -s /bin/bash netdata
bash-4.4$ export NETDATA_ALARM_NOTIFY_DEBUG=1
bash-4.4$ /usr/libexec/netdata/plugins.d/alarm-notify.sh test
bash: /usr/libexec/netdata/plugins.d/alarm-notify.sh: No such file or directory
bash-4.4$ /opt/netdata/usr/libexec/netdata/plugins.d/alarm-notify.sh test 
 ```

## Config de la backup
Installation des paquets
`sudo dnf -y install nfs-utils`

On modifie  le fichier /etc/idmapd.conf en décommentant la ligne Domain = puis en la modifiant comme ceci : `Domain = tp3.linux` avec 
`sudo vim /etc/idmapd.conf` 

On modifie le fichier hosts pour ajouter l'ip de la machine: `10.101.1.12 backup.tp3.linux`,
`sudo vim /etc/hosts`

On crée le dossier pour le partage
`sudo mkdir -p /srv/backup`

On monte ensuite le partage de fichier:
`sudo mount -t nfs backup.tp3.linux:/srv/backup/bot.tp3.linux /srv/backup`

Script pour le backup:
`sudo vim /srv/tp2_backup.sh`
```
#!/bin/bash
# Backup script
# Kraizix - 25/10/2021
fname=$(date +'%y%m%d_%H%M%S')
tar -zcvf tp2_backup_${fname}.tar.gz $2
rsync -av --remove-source-files tp2_backup_${fname}.tar.gz $1
ls -tp $1 | grep -v '/$' | tail -n +6 | xargs -I {} rm -- $1/{}
```
On ajoute les droits d'éxecution au script:
`sudo chmod +x /srv/tp2_backup.sh`
Création du service :
`sudo vim tp3_backup.service`
```
[Unit]
Description=Our own lil backup service (TP3)

[Service]
ExecStart=/srv/tp2_backup.sh /srv/backup /usr/bin/bot/
Type=oneshot
RemainAfterExit=no

[Install]
WantedBy=multi-user.target
```

Création du timer :
`vim tp3_backup.timer`
```
[Unit]
Description=Periodically run our TP2 backup script
Requires=tp3_backup.service

[Timer]
Unit=tp3_backup.service
OnCalendar=*-*-* 3:15:00

[Install]
WantedBy=timers.target
```

On active le timer :
```
sudo systemctl start tp3_backup.timer
sudo systemctl enable tp3_backup.timer

```

Vérification :
```
[kraizix@bot system]$ sudo systemctl status tp3_backup.service
● tp3_backup.service - Our own lil backup service (TP3)
   Loaded: loaded (/etc/systemd/system/tp3_backup.service; disabled; vendor>
   Active: inactive (dead) since Sun 2021-11-14 13:47:33 CET; 4min 33s ago
  Process: 41264 ExecStart=/srv/tp2_backup.sh /srv/backup /var/www/web.tp3.>
 Main PID: 41264 (code=exited, status=0/SUCCESS)

Nov 14 13:47:32 bot.tp3.linux tp2_backup.sh[41264]: /var/www/web.tp>
Nov 14 13:47:32 bot.tp3.linux tp2_backup.sh[41264]: /var/www/web.tp>
Nov 14 13:47:32 bot.tp3.linux tp2_backup.sh[41264]: /var/www/web.tp>
Nov 14 13:47:32 bot.tp3.linux tp2_backup.sh[41264]: /var/www/web.tp>
Nov 14 13:47:32 bot.tp3.linux tp2_backup.sh[41264]: sending increme>
Nov 14 13:47:32 bot.tp3.linux tp2_backup.sh[41264]: tp2_backup_2111>
Nov 14 13:47:32 bot.tp3.linux tp2_backup.sh[41264]: sent 268,610 by>
Nov 14 13:47:32 bot.tp3.linux tp2_backup.sh[41264]: total size is 2>
Nov 14 13:47:33 bot.tp3.linux systemd[1]: tp3_backup.service: Succe>
Nov 14 13:47:33 bot.tp3.linux systemd[1]: Started Our own lil backu>

```

# III. Bot dashboard

[Lien du github de l'interface](https://github.com/Notavone/discord-panel)

Mise à jour des paquets de la machine : 
`sudo dnf update -y`

On installe apache et wget pour la suite de la config : 
`sudo dnf install httpd wget -y`

On crée le dossier qui contiendra les fichiers du serveur : 
`sudo mkdir -p /var/www/web.tp3.linux/public_html`

Puis on se rend dans le dossier :
`cd /var/www/web.tp3.linux/public_html`

On récupère les fichiers du dashboard :
`sudo wget https://github.com/Notavone/discord-panel/archive/refs/tags/1.3.9.tar.gz`

On décompresse ensuite le fichier obtenu :
`sudo tar -xf 1.3.9.tar.gz`

On déplace ensuite fichiers que l'on a décompresser et on les déplace dans public_html et on supprime les fichiers non nécessaires
```
sudo cp -a discord-panel-1.3.9/. .
sudo rm -r discord-panel-1.3.9/ 
sudo rm 1.3.9.tar.gz
```
On donne les droits sur les fichiers à l'utilisateur apache :
`sudo chown -Rf apache.apache /var/www/web.tp3.linux/public_html/`

`sudo chmod -R 755 /var/www/web.tp3.linux/`

On modifie le fichier de conf d'apache :
`sudo vi /etc/httpd/conf/httpd.conf`
 et on ajoute ça : `Include web.tp3.linux.d/*.conf`


Puis on crée le dossier de conf :
`sudo mkdir /etc/httpd/web.tp3.linux.d`

On crée un fichier qui sera le template du fichier de conf du site
`sudo vi /etc/httpd/web.tp3.linux.d/default.template`

```
<VirtualHost *:80>
        ServerName tp3.linux
        ServerAlias web.tp3.linux
        DocumentRoot /var/www/web.tp3.linux/public_html
</VirtualHost>
```
On crée ensuite le fichier de conf
`sudo cp /etc/httpd/web.tp3.linux.d/default.template /etc/httpd/web.tp3.linux.d/tp3.linux.conf`


On restart apache
`sudo systemctl restart httpd`

On ajoute le port 80 au firewall:
```
sudo firewall-cmd --add-port=80/tcp
sudo firewall-cmd --add-port=80/tcp --permanent
```


On modifie le fichier hosts de windows qui se trouve dans C:\Windows\System32\drivers\etc :
`10.101.1.16 linux.tp3`

Résultat :
![](dashboard.png)
## Installation du monitoring

Pour le monitoring de la machine on utilise Netdata

```bash
sudo -
bash <(curl -Ss https://my-netdata.io/kickstart-static64.sh)
exit
```

On ajoute le port de netdata puis on reload
```
firewall-cmd  --add-port=19999/tcp
firewall-cmd  --add-port=19999/tcp --permanent
firewall-cmd --reload
```

Optionnel - Ajout de l'alerting via discord :
`sudo /opt/netdata//etc/netdata/edit-config health_alarm_notify.conf`
 modifier la ligne DISCORD_WEBHOOK_URL="" et ajoutez l'url de votre webhook puis ajouter le channel ici DEFAULT_RECIPIENT_DISCORD=""

 Test de l'alerting :
 ```
 [kraizix@bot ~]$ sudo su -s /bin/bash netdata
bash-4.4$ export NETDATA_ALARM_NOTIFY_DEBUG=1
bash-4.4$ /opt/netdata/usr/libexec/netdata/plugins.d/alarm-notify.sh test 
 ```

## Config de la backup
Installation des paquets
`sudo dnf -y install nfs-utils`

On modifie  le fichier /etc/idmapd.conf en décommentant la ligne Domain = puis en la modifiant comme ceci : `Domain = tp3.linux` avec 
`sudo vim /etc/idmapd.conf` 

On modifie le fichier hosts pour ajouter l'ip de la machine: `10.101.1.12 backup.tp3.linux`,
`sudo vim /etc/hosts`

On crée le dossier pour le partage
`sudo mkdir -p /srv/backup`

On monte ensuite le partage de fichier:
`sudo mount -t nfs backup.tp3.linux:/srv/backup/web.tp3.linux /srv/backup`

Script pour le backup:
`sudo vim /srv/tp2_backup.sh`
```
#!/bin/bash
# Backup script
# Kraizix - 25/10/2021
fname=$(date +'%y%m%d_%H%M%S')
tar -zcvf tp2_backup_${fname}.tar.gz $2
rsync -av --remove-source-files tp2_backup_${fname}.tar.gz $1
ls -tp $1 | grep -v '/$' | tail -n +6 | xargs -I {} rm -- $1/{}
```
On ajoute les droits d'éxecution au script:
`sudo chmod +x /srv/tp2_backup.sh`
Création du service :
`sudo vim tp3_backup.service`
```
[Unit]
Description=Our own lil backup service (TP3)

[Service]
ExecStart=/srv/tp2_backup.sh /srv/backup /var/www/web.tp3.linux
Type=oneshot
RemainAfterExit=no

[Install]
WantedBy=multi-user.target
```

Création du timer :
`vim tp3_backup.timer`
```
[Unit]
Description=Periodically run our TP2 backup script
Requires=tp3_backup.service

[Timer]
Unit=tp3_backup.service
OnCalendar=*-*-* 3:15:00

[Install]
WantedBy=timers.target
```

On active le timer :
```
sudo systemctl start tp3_backup.timer
sudo systemctl enable tp3_backup.timer
```

# IV. Récap


| Machine            | IP            | Service                 | Port ouvert |
| ------------------ | ------------- | ----------------------- | ----------- |
| `bot.tp3.linux`    | `10.101.1.15` | Bot Discord             | 19999,22    |
| `web.tp3.linux`    | `10.101.1.16` | Serveur web             | 80,19999,22 |
| `backup.tp3.linux` | `10.101.1.12` | Serveur de Backup (NFS) | 2049,22     |

