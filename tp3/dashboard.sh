#!/bin/bash
# Install script for discord dashboards
# Kraizix - 14/11/2021
dnf update -y
dnf install -y httpd wget
mkdir -p /var/www/web.tp3.linux/public_html
wget https://github.com/Notavone/discord-panel/archive/refs/tags/1.3.9.tar.gz
tar -xf 1.3.9.tar.gz
cp -a discord-panel-1.3.9/. /var/www/web.tp3.linux/public_html
rm -r discord-panel-1.3.9/ 
rm 1.3.9.tar.gz
chown -Rf apache.apache /var/www/web.tp3.linux/public_html/
chmod -R 755 /var/www/web.tp3.linux/
echo "Include web.tp3.linux.d/*.conf" >> /etc/httpd/conf/httpd.conf
mkdir /etc/httpd/web.tp3.linux.d
cat <<-EOF! > /etc/httpd/web.tp3.linux.d/default.template
<VirtualHost *:80>
        ServerName tp3.linux
        ServerAlias web.tp3.linux
        DocumentRoot /var/www/web.tp3.linux/public_html
</VirtualHost>
EOF!
cp /etc/httpd/web.tp3.linux.d/default.template /etc/httpd/web.tp3.linux.d/tp3.linux.conf
systemctl restart httpd
bash <(curl -Ss https://my-netdata.io/kickstart-static64.sh)
/opt/netdata//etc/netdata/edit-config health_alarm_notify.conf
read -p "Insert Discord Webhook link: " webhook
read -p "Insert Discord Alert Default Channel: " chan
sed -i -e "\,DISCORD_WEBHOOK_URL=, s,=.*,=\"${webhook}\"," /opt/netdata/etc/netdata/health_alarm_notify.conf
sed -i -e "/DEFAULT_RECIPIENT_DISCORD=/ s/=.*/=\"${chan}\"/" /opt/netdata/etc/netdata/health_alarm_notify.conf
sed -i 's/curl=""/curl="\/opt\/netdata\/bin\/curl -k"/' /opt/netdata/etc/netdata/health_alarm_notify.conf

firewall-cmd --add-port=19999/tcp
firewall-cmd --add-port=19999/tcp --permanent
firewall-cmd --add-port=80/tcp --permanent
firewall-cmd --add-port=80/tcp
firewall-cmd --reload
dnf install -y nfs-utils
sed -i '5i Domain = tp3.linux' /etc/idmapd.conf
echo '10.101.1.12 backup.tp3.linux'>> /etc/hosts
mkdir -p /srv/backup
mount -t nfs backup.tp3.linux:/srv/backup/web.tp3.linux /srv/backup
cat <<-EOF! > /srv/tp2_backup.sh
#!/bin/bash
# Backup script
# Kraizix - 25/10/2021
fname=\$(date +'%y%m%d_%H%M%S')
tar -zcvf tp2_backup_\${fname}.tar.gz \$2
rsync -av --remove-source-files tp2_backup_\${fname}.tar.gz \$1
ls -tp \$1 | grep -v '/$' | tail -n +6 | xargs -I {} rm -- \$1/{}
EOF!
chmod +x /srv/tp2_backup.sh
cat <<-EOF! > /etc/systemd/system/tp3_backup.service
[Unit]
Description=Our own lil backup service (TP3)

[Service]
ExecStart=/srv/tp2_backup.sh /srv/backup /var/www/web.tp3.linux
Type=oneshot
RemainAfterExit=no

[Install]
WantedBy=multi-user.target
EOF!
cat <<-EOF! > /etc/systemd/system/tp3_backup.timer
[Unit]
Description=Periodically run our TP2 backup script
Requires=tp3_backup.service

[Timer]
Unit=tp3_backup.service
OnCalendar=*-*-* 3:15:00

[Install]
WantedBy=timers.target
EOF!
systemctl daemon-reload
systemctl start tp3_backup.timer
systemctl enable tp3_backup.timer
