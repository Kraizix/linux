#!/bin/bash
# Install script for backup
# Kraizix - 14/11/2021
sudo dnf install -y nfs-utils
sed -i '5i Domain = tp3.linux' /etc/idmapd.conf
echo '/srv/backup 10.101.1.0/24(rw,no_root_squash)'>> /etc/exports
mkdir -p /srv/backup/web.tp3.linux
mkdir -p /srv/backup/bot.tp3.linux
systemctl enable --now rpcbind nfs-server
bash <(curl -Ss https://my-netdata.io/kickstart-static64.sh)
/opt/netdata//etc/netdata/edit-config health_alarm_notify.conf
read -p "Insert Discord Webhook link: " webhook
read -p "Insert Discord Alert Default Channel: " chan
sed -i -e "\,DISCORD_WEBHOOK_URL=, s,=.*,=\"${webhook}\"," /opt/netdata/etc/netdata/health_alarm_notify.conf
sed -i -e "/DEFAULT_RECIPIENT_DISCORD=/ s/=.*/=\"${chan}\"/" /opt/netdata/etc/netdata/health_alarm_notify.conf
firewall-cmd --add-port=19999/tcp
firewall-cmd --add-port=19999/tcp --permanent
firewall-cmd --add-port=2049/tcp --permanent
firewall-cmd --add-port=2049/tcp
firewall-cmd --reload