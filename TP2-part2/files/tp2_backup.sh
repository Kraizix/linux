#!/bin/bash
# Backup script
# Kraizix - 25/10/2021
fname=$(date +'%y%m%d_%H%M%S')
tar -zcvf tp2_backup_${fname}.tar.gz $2
rsync -av --remove-source-files tp2_backup_${fname}.tar.gz $1
ls -tp $1 | grep -v '/$' | tail -n +6 | xargs -I {} rm -- $1/{}
