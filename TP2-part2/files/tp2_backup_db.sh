#!/bin/bash
# Backup script for db
# Kraizix - 25/10/2021
fname=$(date +'%y%m%d_%H%M%S')
mysqldump --user=root --password=root --databases $2 > backup_db.sql 
tar -zcvf tp2_backup_${fname}.tar.gz backup_db.sql
rsync -av --remove-source-files tp2_backup_${fname}.tar.gz $1
rm backup_db.sql
ls -tp $1 | grep -v '/$' | tail -n +6 | xargs -I {} rm -- $1/{}
