# TP2 pt. 2 : Maintien en condition opérationnelle

# Sommaire

- [TP2 pt. 2 : Maintien en condition opérationnelle](#tp2-pt-2--maintien-en-condition-opérationnelle)
- [Sommaire](#sommaire)
- [I. Monitoring](#i-monitoring)
  - [1. Le concept](#1-le-concept)
  - [2. Setup](#2-setup)
- [II. Backup](#ii-backup)
  - [1. Intwo bwo](#1-intwo-bwo)
  - [2. Partage NFS](#2-partage-nfs)
  - [3. Backup de fichiers](#3-backup-de-fichiers)
  - [4. Unité de service](#4-unité-de-service)
    - [A. Unité de service](#a-unité-de-service)
    - [B. Timer](#b-timer)
    - [C. Contexte](#c-contexte)
  - [5. Backup de base de données](#5-backup-de-base-de-données)
  - [6. Petit point sur la backup](#6-petit-point-sur-la-backup)
- [III. Reverse Proxy](#iii-reverse-proxy)
  - [1. Introooooo](#1-introooooo)
  - [2. Setup simple](#2-setup-simple)
  - [3. Bonus HTTPS](#3-bonus-https)
- [IV. Firewalling](#iv-firewalling)
  - [1. Présentation de la syntaxe](#1-présentation-de-la-syntaxe)
  - [2. Mise en place](#2-mise-en-place)
    - [A. Base de données](#a-base-de-données)
    - [B. Serveur Web](#b-serveur-web)
    - [C. Serveur de backup](#c-serveur-de-backup)
    - [D. Reverse Proxy](#d-reverse-proxy)
    - [E. Tableau récap](#e-tableau-récap)
# I. Monitoring

On bouge pas pour le moment niveau machines :

| Machine         | IP            | Service                 | Port ouvert | IPs autorisées |
| --------------- | ------------- | ----------------------- | ----------- | -------------- |
| `web.tp2.linux` | `10.102.1.11` | Serveur Web             | 80          | toutes         |
| `db.tp2.linux`  | `10.102.1.12` | Serveur Base de Données | 3306        | 10.102.1.11    |

## 1. Le concept

## 2. Setup
On vérifie que le service est actif :
```
[kraizix@web ~]$ systemctl is-active netdata
active
```
On  vérifie que le service est démarré au boot de la machine :
```
[kraizix@web ~]$ systemctl is-enabled netdata
enabled
```
On vérifie le port sur  lequel écoute netdata ( ici le port 19999)
```
[kraizix@web ~]$ sudo ss -alnpt
[sudo] password for kraizix:   0.0.0.0:19999         0.0.0.0:*
State         Recv-Q         Send-Q                 Local Address:Port                  Peer Address:Port        Process
LISTEN        0              128                          0.0.0.0:22                         0.0.0.0:*            users:(("sshd",pid=756,fd=5))
LISTEN        0              128                        127.0.0.1:8125                       0.0.0.0:*            users:(("netdata",pid=3392,fd=35))
LISTEN        0              128                          0.0.0.0:19999                      0.0.0.0:*            users:(("netdata",pid=3392,fd=5))
LISTEN        0              128                             [::]:22                            [::]:*            users:(("sshd",pid=756,fd=7))
LISTEN        0              128                            [::1]:8125                          [::]:*            users:(("netdata",pid=3392,fd=34))
LISTEN        0              128                             [::]:19999                         [::]:*            users:(("netdata",pid=3392,fd=6))
LISTEN        0              128                                *:80                               *:*            users:(("httpd",pid=2635,fd=4),("httpd",pid=2421,fd=4),("httpd",pid=2420,fd=4),("httpd",pid=2419,fd=4),("httpd",pid=752,fd=4))

```
Modification des ports: 
```
[kraizix@web ~]$ sudo firewall-cmd --add-port=19999/tcp --permanent
success
[kraizix@web ~]$ sudo firewall-cmd --add-port=19999/tcp
success

```
Setup alerting :

Modif du fichier de conf en ajoutant le webhook discord ainsi que le channel dans lequel le bot devra écrire
```
sudo /opt/netdata//etc/netdata/edit-config health_alarm_notify.conf
```

```
DISCORD="YES"

# Create a webhook by following the official documentation -
# https://support.discordapp.com/hc/en-us/articles/228383668-Intro-to-Webhooks
DISCORD_WEBHOOK_URL="https://discord.com/api/webhooks/897106597736763393/4uE0PgbkVIeBv8Y0xmCUxMP3o95BbXZa1QiN6Ea0UL9XbJSM37T22CA3pxUjKslUPQfN"

# if a role's recipients are not configured, a notification will be send to
# this discord channel (empty = do not send a notification for unconfigured
# roles):
DEFAULT_RECIPIENT_DISCORD="oui"

```

test de l'alerte :
```
[kraizix@web srv]$ sudo su -s /bin/bash netdata
bash-4.4$ export NETDATA_ALARM_NOTIFY_DEBUG=1
bash-4.4$ /opt/netdata/usr/libexec/netdata/plugins.d/alarm-notify.sh test

# SENDING TEST WARNING ALARM TO ROLE: sysadmin
2021-10-25 23:59:08: alarm-notify.sh: DEBUG: Loading config file '/opt/netdata/usr/lib/netdata/conf.d/health_alarm_notify.conf'...
2021-10-25 23:59:08: alarm-notify.sh: DEBUG: Loading config file '/opt/netdata/etc/netdata/health_alarm_notify.conf'...
2021-10-25 23:59:08: alarm-notify.sh: DEBUG: Cannot find sendmail command in the system path. Disabling email notifications.
2021-10-25 23:59:08: alarm-notify.sh: DEBUG: Cannot find aws command in the system path.  Disabling Amazon SNS notifications.
--- BEGIN curl command ---
/opt/netdata/bin/curl -k -X POST --data-urlencode $'payload=        {\n            "channel": "#oui",\n            "username": "netdata on web.tp2.linux",\n            "text": "web.tp2.linux needs attention, `test.chart` (_test.family_), *test alarm = new value*",\n            "icon_url": "https://registry.my-netdata.io/images/banner-icon-144x144.png",\n            "attachments": [\n                {\n                    "color": "warning",\n
          "title": "test alarm = new value",\n                    "title_link": "https://app.netdata.cloud/alarms/redirect?agentId=a6f978fa-2a06-11ec-bef4-080027a0bf15&host=web.tp2.linux&chart=test.chart&family=test.family&alarm=test_alarm&alarm_unique_id=1&alarm_id=1&alarm_event_id=1&alarm_when=1635199148&alarm_status=WARNING&alarm_chart=test.chart&alarm_value=new%20value",\n                    "text": "this is a test alarm to verify notifications work",\n                    "fields": [\n                        {\n
                 "title": "test.chart",\n                            "value": "test.family"\n                        }\n                    ],\n                    "thumb_url": "https://registry.my-netdata.io/images/alert-128-orange.png",\n                    "footer_icon": "https://registry.my-netdata.io/images/banner-icon-144x144.png",\n                    "footer": "web.tp2.linux",\n                    "ts": 1635199148\n                }\n
  ]\n        }' https://discord.com/api/webhooks/897106597736763393/4uE0PgbkVIeBv8Y0xmCUxMP3o95BbXZa1QiN6Ea0UL9XbJSM37T22CA3pxUjKslUPQfN/slack
--- END curl command ---
--- BEGIN received response ---
ok
--- END received response ---
RECEIVED HTTP RESPONSE CODE: 200
2021-10-25 23:59:08: alarm-notify.sh: INFO: sent discord notification for: web.tp2.linux test.chart.test_alarm is WARNING to 'oui'
# OK

# SENDING TEST CRITICAL ALARM TO ROLE: sysadmin
2021-10-25 23:59:08: alarm-notify.sh: DEBUG: Loading config file '/opt/netdata/usr/lib/netdata/conf.d/health_alarm_notify.conf'...
2021-10-25 23:59:08: alarm-notify.sh: DEBUG: Loading config file '/opt/netdata/etc/netdata/health_alarm_notify.conf'...
2021-10-25 23:59:08: alarm-notify.sh: DEBUG: Cannot find sendmail command in the system path. Disabling email notifications.
2021-10-25 23:59:08: alarm-notify.sh: DEBUG: Cannot find aws command in the system path.  Disabling Amazon SNS notifications.
--- BEGIN curl command ---
/opt/netdata/bin/curl -k -X POST --data-urlencode $'payload=        {\n            "channel": "#oui",\n            "username": "netdata on web.tp2.linux",\n            "text": "web.tp2.linux is critical, `test.chart` (_test.family_), *test alarm = new value*",\n            "icon_url": "https://registry.my-netdata.io/images/banner-icon-144x144.png",\n            "attachments": [\n                {\n                    "color": "danger",\n
     "title": "test alarm = new value",\n                    "title_link": "https://app.netdata.cloud/alarms/redirect?agentId=a6f978fa-2a06-11ec-bef4-080027a0bf15&host=web.tp2.linux&chart=test.chart&family=test.family&alarm=test_alarm&alarm_unique_id=1&alarm_id=1&alarm_event_id=2&alarm_when=1635199148&alarm_status=CRITICAL&alarm_chart=test.chart&alarm_value=new%20value",\n                    "text": "this is a test alarm to verify notifications work",\n                    "fields": [\n                        {\n
             "title": "test.chart",\n                            "value": "test.family"\n                        }\n                    ],\n
        "thumb_url": "https://registry.my-netdata.io/images/alert-128-red.png",\n                    "footer_icon": "https://registry.my-netdata.io/images/banner-icon-144x144.png",\n                    "footer": "web.tp2.linux",\n                    "ts": 1635199148\n                }\n            ]\n        }' https://discord.com/api/webhooks/897106597736763393/4uE0PgbkVIeBv8Y0xmCUxMP3o95BbXZa1QiN6Ea0UL9XbJSM37T22CA3pxUjKslUPQfN/slack
--- END curl command ---
--- BEGIN received response ---
ok
--- END received response ---
RECEIVED HTTP RESPONSE CODE: 200
2021-10-25 23:59:09: alarm-notify.sh: INFO: sent discord notification for: web.tp2.linux test.chart.test_alarm is CRITICAL to 'oui'
# OK

# SENDING TEST CLEAR ALARM TO ROLE: sysadmin
2021-10-25 23:59:09: alarm-notify.sh: DEBUG: Loading config file '/opt/netdata/usr/lib/netdata/conf.d/health_alarm_notify.conf'...
2021-10-25 23:59:09: alarm-notify.sh: DEBUG: Loading config file '/opt/netdata/etc/netdata/health_alarm_notify.conf'...
2021-10-25 23:59:09: alarm-notify.sh: DEBUG: Cannot find sendmail command in the system path. Disabling email notifications.
2021-10-25 23:59:09: alarm-notify.sh: DEBUG: Cannot find aws command in the system path.  Disabling Amazon SNS notifications.
--- BEGIN curl command ---
/opt/netdata/bin/curl -k -X POST --data-urlencode $'payload=        {\n            "channel": "#oui",\n            "username": "netdata on web.tp2.linux",\n            "text": "web.tp2.linux recovered, `test.chart` (_test.family_), *test alarm (alarm was raised for 3 seconds)*",\n            "icon_url": "https://registry.my-netdata.io/images/banner-icon-144x144.png",\n
   "attachments": [\n                {\n                    "color": "good",\n                    "title": "test alarm (alarm was raised for 3 seconds)",\n                    "title_link": "https://app.netdata.cloud/alarms/redirect?agentId=a6f978fa-2a06-11ec-bef4-080027a0bf15&host=web.tp2.linux&chart=test.chart&family=test.family&alarm=test_alarm&alarm_unique_id=1&alarm_id=1&alarm_event_id=3&alarm_when=1635199149&alarm_status=CLEAR&alarm_chart=test.chart&alarm_value=new%20value",\n                    "text": "this is a test alarm to verify notifications work",\n                    "fields": [\n                        {\n                            "title": "test.chart",\n                            "value": "test.family"\n                        }\n                    ],\n                    "thumb_url": "https://registry.my-netdata.io/images/check-mark-2-128-green.png",\n                    "footer_icon": "https://registry.my-netdata.io/images/banner-icon-144x144.png",\n                    "footer": "web.tp2.linux",\n                    "ts": 1635199149\n                }\n            ]\n        }' https://discord.com/api/webhooks/897106597736763393/4uE0PgbkVIeBv8Y0xmCUxMP3o95BbXZa1QiN6Ea0UL9XbJSM37T22CA3pxUjKslUPQfN/slack
--- END curl command ---
--- BEGIN received response ---
ok
--- END received response ---
RECEIVED HTTP RESPONSE CODE: 200
2021-10-25 23:59:10: alarm-notify.sh: INFO: sent discord notification for: web.tp2.linux test.chart.test_alarm is CLEAR to 'oui'
# OK
```


Création de l'alerte de ram :
on doit d'abord se rendre dans le répertoire `/opt/netdata/etc/netdata`
```
[kraizix@web netdata]$ sudo touch health.d/ram-usage.conf
[kraizix@web netdata]$ sudo ./edit-config health.d/ram-usage.conf
```

```

 alarm: ram_usage
    on: system.ram
lookup: average -10s percentage of used
 units: %
 every: 20s
  warn: $this > 50
  crit: $this > 60
  info: The percentage of RAM being used by the system.

```
Commande pour simuler l'utilisation de 600mo de ram :
```
sudo stress -m 1 --vm-bytes 600M
```

On reçoit l'alerte suivant sur discord : 
```
db.tp2.liux needs attention, system.ram (ram), ram usage = 50.6%
```
# II. Backup

| Machine            | IP            | Service                 | Port ouvert | IPs autorisées |
| ------------------ | ------------- | ----------------------- | ----------- | -------------- |
| `web.tp2.linux`    | `10.102.1.11` | Serveur Web             | ?           | ?              |
| `db.tp2.linux`     | `10.102.1.12` | Serveur Base de Données | ?           | ?              |
| `backup.tp2.linux` | `10.102.1.13` | Serveur de Backup (NFS) | ?           | ?              |


## 1. Intwo bwo

## 2. Partage NFS

Configuration du serveur nfs :

- `sudo dnf -y install nfs-utils`
- `sudo vim /etc/idmapd.conf` on décommente la ligne Domain et on la modifie comme cela :
```
Domain = tp2.linux
```
- `sudo vim /etc/exports` et on ajoute cette lign : `/srv/backup 10.102.1.0/24(rw,no_root_squash)`
- On ajoute les ports au firewall :
```
sudo firewall-cmd --add-port=19999/tcp
sudo firewall-cmd --add-port=19999/tcp --permanent
sudo firewall-cmd --add-port=2049/tcp
sudo firewall-cmd --add-port=2049/tcp --permanent
```
Config client nfs :

- `sudo dnf -y install nfs-utils`
- `sudo vim /etc/idmapd.conf` on décommente la ligne Domain et on la modifie comme cela :
```
Domain = tp2.linux
```
- sudo mount -t nfs backup.tp2.linux:/srv/backup/web.tp2.linux /srv/backup

commande mount:
```
[kraizix@web backup]$ mount | grep backup
backup.tp2.linux:/srv/backup/web.tp2.linux on /srv/backup type nfs4 (rw,relatime,vers=4.2,rsize=131072,wsize=131072,namlen=255,hard,proto=tcp,timeo=600,retrans=2,sec=sys,clientaddr=10.102.1.11,local_lock=none,addr=10.102.1.13)
```

commande df -h:
```
[kraizix@web backup]$ df -h
Filesystem                                  Size  Used Avail Use% Mounted on
devtmpfs                                    387M     0  387M   0% /dev
tmpfs                                       405M  352K  405M   1% /dev/shm
tmpfs                                       405M  5.6M  400M   2% /run
tmpfs                                       405M     0  405M   0% /sys/fs/cgroup
/dev/mapper/rl-root                         6.2G  3.6G  2.7G  57% /
/dev/sda1                                  1014M  241M  774M  24% /boot
tmpfs                                        81M     0   81M   0% /run/user/1000
backup.tp2.linux:/srv/backup/web.tp2.linux  6.2G  2.3G  4.0G  37% /srv/backup
```

Commande touch :
```
[kraizix@web backup]$ sudo touch file1
[kraizix@web backup]$ ls
file1
```
Montage automatique:
```
[kraizix@web backup]$ cat /etc/fstab

#
# /etc/fstab
# Created by anaconda on Wed Sep 15 13:25:17 2021
#
# Accessible filesystems, by reference, are maintained under '/dev/disk/'.
# See man pages fstab(5), findfs(8), mount(8) and/or blkid(8) for more info.
#
# After editing this file, run 'systemctl daemon-reload' to update systemd
# units generated from this file.
#
/dev/mapper/rl-root     /                       xfs     defaults        0 0
UUID=44569c3d-979b-4db7-94fd-740d13789a72 /boot                   xfs     defaults        0 0
/dev/mapper/rl-swap     none                    swap    defaults        0 0
backup.tp2.linux:/srv/backup/web.tp2.linux /srv/backup  nfs     defaults   0 0
```
## 3. Backup de fichiers
```
[kraizix@web ~]$ cat tp2_backup
#!/bin/bash
# Backup script
# Kraizix - 25/10/2021
fname=$(date +'%y%m%d_%H%M%S')
tar -zcvf tp2_backup_${fname}.tar.gz $2
rsync -av --remove-source-files tp2_backup_${fname}.tar.gz $1
ls -tp $1 | grep -v '/$' | tail -n +6 | xargs -I {} rm -- $1/{}
```

Test du script : 

```
[kraizix@web ~]$ ./tp2_backup.sh backup/ test/
test/
test/file1
sending incremental file list
tp2_backup_211025_201632.tar.gz

sent 281 bytes  received 43 bytes  648.00 bytes/sec
total size is 146  speedup is 0.45
[kraizix@web backup]$ ls -l
total 4
-rw-rw-r--. 1 kraizix kraizix 146 Oct 25 20:16 tp2_backup_211025_201632.tar.gz
```
Vérif du backup : 
```
[kraizix@web backup]$ tar -xzf tp2_backup_211025_201632.tar.gz
[kraizix@web backup]$ ls
test  tp2_backup_211025_201632.tar.gz
[kraizix@web backup]$ ls -l test/
total 0
-rw-rw-r--. 1 kraizix kraizix 0 Oct 11 04:18 file1
```
📁 Fichier [/srv/tp2_backup.sh](files/tp2_backup.sh)

Bonus :
Suppression de l'archive la plus vieille lors de l'éxécution du script quand le nombre d'archive dépasse 5

On ajoute la ligne suivant au script :
`ls -tp $1 | grep -v '/$' | tail -n +6 | xargs -I {} rm -- $1/{}`
résultat :
```
[kraizix@web srv]$ ls backup/
tp2_backup_211025_205004.tar.gz  tp2_backup_211025_234546.tar.gz
tp2_backup_211025_205104.tar.gz  tp2_backup_211025_234648.tar.gz
tp2_backup_211025_205224.tar.gz
[kraizix@web srv]$ sudo systemctl start tp2_backup.service
[kraizix@web srv]$ ls backup/
tp2_backup_211025_205104.tar.gz  tp2_backup_211025_234648.tar.gz
tp2_backup_211025_205224.tar.gz  tp2_backup_211025_234729.tar.gz
tp2_backup_211025_234546.tar.gz

```
## 4. Unité de service

### A. Unité de service
Script de l'unité de service du backup :
`vim /etc/systemd/system/tp2_backup.service`

```
[Unit]
Description=Our own lil backup service (TP2)

[Service]
ExecStart=/srv/tp2_backup.sh /srv/backup /home/kraizix/nextcloud
Type=oneshot
RemainAfterExit=no

[Install]
WantedBy=multi-user.target
```
Test du service
```
[kraizix@web ~]$ sudo systemctl start tp2_backup
[kraizix@web ~]$ ls /srv/backup
file1  tp2_backup_211025_202425.tar.gz
```

### B. Timer
Démarrage du service :

```
[kraizix@web ~]$ sudo systemctl start tp2_backup.timer
[kraizix@web ~]$ sudo systemctl enable tp2_backup.timer
```

Vérification que le service est allumé :

```
[kraizix@web ~]$ sudo systemctl is-active tp2_backup.timer
active
[kraizix@web ~]$ sudo systemctl is-enabled tp2_backup.timer
enabled
```

Vérification que le backup a bien fonctionné :
```
[kraizix@web ~]$ systemctl status tp2_backup.service
● tp2_backup.service - Our own lil backup service (TP2)
   Loaded: loaded (/etc/systemd/system/tp2_backup.service; disabled; vendor preset: disabled)
   Active: inactive (dead) since Mon 2021-10-25 20:36:25 CEST; 18s ago
  Process: 3026 ExecStart=/srv/tp2_backup.sh /srv/backup /home/kraizix/nextcloud (code=exited, status=0/SUCCESS)
 Main PID: 3026 (code=exited, status=0/SUCCESS)

Oct 25 20:36:17 web.tp2.linux tp2_backup.sh[3026]: /home/kraizix/nextcloud/apps/twofactor_backupcodes/l10n/ka_GE.js
Oct 25 20:36:17 web.tp2.linux tp2_backup.sh[3026]: /home/kraizix/nextcloud/apps/twofactor_backupcodes/l10n/cs_CZ.js
Oct 25 20:36:17 web.tp2.linux tp2_backup.sh[3026]: /home/kraizix/nextcloud/apps/twofactor_backupcodes/l10n/de_DE.json
Oct 25 20:36:17 web.tp2.linux tp2_backup.sh[3026]: /home/kraizix/nextcloud/apps/twofactor_backupcodes/l10n/sq.js
Oct 25 20:36:17 web.tp2.linux tp2_backup.sh[3026]: /home/kraizix/nextcloud/apps/twofactor_backupcodes/l10n/pt_BR.js
Oct 25 20:36:17 web.tp2.linux tp2_backup.sh[3026]: /home/kraizix/nextcloud/apps/twofactor_backupcodes/l10n/he.js
Oct 25 20:36:17 web.tp2.linux tp2_backup.sh[3026]: /home/kraizix/nextcloud/apps/twofactor_backupcodes/l10n/eu.json
Oct 25 20:36:17 web.tp2.linux tp2_backup.sh[3026]: /home/kraizix/nextcloud/apps/twofactor_backupcodes/l10n/es_AR.json
Oct 25 20:36:25 web.tp2.linux systemd[1]: tp2_backup.service: Succeeded.
Oct 25 20:36:25 web.tp2.linux systemd[1]: Started Our own lil backup service (TP2).
```

### C. Contexte
```
kraizix@web html]$ sudo systemctl list-timers
NEXT                          LEFT     LAST                          PASSED    UNIT                         ACTIVATES
Tue 2021-10-26 03:15:00 CEST  6h left  n/a                           n/a       tp2_backup.timer             tp2_backup.service
Tue 2021-10-26 20:07:45 CEST  23h left Mon 2021-10-25 20:07:45 CEST  44min ago systemd-tmpfiles-clean.timer systemd-tmpfiles-clean.service
n/a                           n/a      n/a                           n/a       dnf-makecache.timer          dnf-makecache.service

3 timers listed.
Pass --all to see loaded but inactive timers, too.
```

📁 Fichier [/etc/systemd/system/tp2_backup.timer](files/tp2_backup.timer)

📁 Fichier [/etc/systemd/system/tp2_backup.service](files/tp2_backup.service)

## 5. Backup de base de données
Création du script tp2_backup_db.sh :
```
[kraizix@db ~]$ cat /srv/tp2_backup_db.sh
#!/bin/bash
# Backup script for db
# Kraizix - 25/10/2021
fname=$(date +'%y%m%d_%H%M%S')
mysqldump --user=root --password=root --databases $2 > backup_db.sql
tar -zcvf tp2_backup_${fname}.tar.gz backup_db.sql
rsync -av --remove-source-files tp2_backup_${fname}.tar.gz $1
rm backup_db.sql
```

Création du service tp2_backup_db.service :
```
[kraizix@db ~]$ sudo cat /etc/systemd/system/tp2_backup_db.service
[Unit]
Description=Our own lil backup service (TP2)

[Service]
ExecStart=/srv/tp2_backup_db.sh /srv/backup nextcloud
Type=oneshot
RemainAfterExit=no

[Install]
WantedBy=multi-user.target
```


vérification du lancement du service :
```
[kraizix@db ~]$ sudo systemctl start tp2_backup_db.service
[kraizix@db ~]$ ls /srv/backup
tp2_backup_211025_232229.tar.gz
[kraizix@db ~]$ tar -xvf /srv/backup/tp2_backup_211025_232704.tar.gz
backup_db.sql
```

Création du timer :
```
[kraizix@db ~]$ sudo cat /etc/systemd/system/tp2_backup_db.timer
[Unit]
Description=Periodically run our TP2 backup script
Requires=tp2_backup_db.service

[Timer]
Unit=tp2_backup_db.service
OnCalendar=*-*-* 3:30:00

[Install]
WantedBy=timers.target
```
Démarrage du timer :
```
[kraizix@db ~]$ sudo systemctl start tp2_backup_db.timer
[kraizix@db ~]$ sudo systemctl enable tp2_backup_db.timer
```
Vérification du démarrage du timer :
```
backup_db.sql
[kraizix@db ~]$ sudo systemctl is-active tp2_backup_db.timer
[sudo] password for kraizix:
active
[kraizix@db ~]$ sudo systemctl is-enabled tp2_backup_db.timer
enabled

```

Vérification de l'heure du timer :
```
[kraizix@db ~]$ sudo systemctl list-timers | grep tp2
Tue 2021-10-26 03:30:00 CEST  4h 0min left n/a                           n/a          tp2_backup_db.timer          tp2_backup_db.service

```
📁 Fichier [/srv/tp2_backup_db.sh](files/tp2_backup_db.sh)

📁 Fichier [/etc/systemd/system/tp2_backup_db.timer](files/tp2_backup_db.timer)

📁 Fichier [/etc/systemd/system/tp2_backup_db.service](files/tp2_backup_db.service)

## 6. Petit point sur la backup

# III. Reverse Proxy

## 1. Introooooo

## 2. Setup simple

| Machine            | IP            | Service                 | Port ouvert | IPs autorisées |
| ------------------ | ------------- | ----------------------- | ----------- | -------------- |
| `web.tp2.linux`    | `10.102.1.11` | Serveur Web             | ?           | ?              |
| `db.tp2.linux`     | `10.102.1.12` | Serveur Base de Données | ?           | ?              |
| `backup.tp2.linux` | `10.102.1.13` | Serveur de Backup (NFS) | ?           | ?              |
| `front.tp2.linux`  | `10.102.1.14` | Reverse Proxy           | ?           | ?              |

Ajout des ports au firewall :
```
[kraizix@front ~]$ sudo firewall-cmd --add-port=80/tcp
success
[kraizix@front ~]$ sudo firewall-cmd --add-port=80/tcp --permanent
success
```

Démarrage de Nginx : 
```
[kraizix@front ~]$ systemctl start nginx
==== AUTHENTICATING FOR org.freedesktop.systemd1.manage-units ====
Authentication is required to start 'nginx.service'.
Authenticating as: kraizix
Password:
==== AUTHENTICATION COMPLETE ====
[kraizix@front ~]$ systemctl enable nginx
==== AUTHENTICATING FOR org.freedesktop.systemd1.manage-unit-files ====
Authentication is required to manage system service or unit files.
Authenticating as: kraizix
Password:
==== AUTHENTICATION COMPLETE ====
Created symlink /etc/systemd/system/multi-user.target.wants/nginx.service → /usr/lib/systemd/system/nginx.service.
==== AUTHENTICATING FOR org.freedesktop.systemd1.reload-daemon ====
Authentication is required to reload the systemd state.
Authenticating as: kraizix
Password:
==== AUTHENTICATION COMPLETE ====
```
Vérification du port sur lequel tourne Nginx (port 80):
```
[kraizix@front ~]$ sudo ss -alnpt | grep nginx
LISTEN 0      128          0.0.0.0:80        0.0.0.0:*    users:(("nginx",pid=1443,fd=8),("nginx",pid=1442,fd=8))
LISTEN 0      128             [::]:80           [::]:*    users:(("nginx",pid=1443,fd=9),("nginx",pid=1442,fd=9))
```
Curl depuis le pc :
```
PS C:\Users\kevin> curl 10.102.1.14
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
  <head>
    <title>Test Page for the Nginx HTTP Server on Rocky Linux</title>
```


On parcourt le fichier de conf de nginx :
```
sudo vim /etc/nginx/nginx.conf

```
on repère l'user de nginx : `user nginx;`

On ajoute un fichier de conf : `/etc/nginx/conf.d/web.tp2.linux.conf`
```
[kraizix@front ~]$ cat /etc/nginx/conf.d/web.tp2.linux.conf
server {
    # on demande à NGINX d'écouter sur le port 80 pour notre NextCloud
    listen 80;

    # ici, c'est le nom de domaine utilisé pour joindre l'application
    # ce n'est pas le nom du reverse proxy, mais le nom que les clients devront saisir pour atteindre le site
    server_name web.tp2.linux; # ici, c'est le nom de domaine utilisé pour joindre l'application (pas forcéme

    # on définit un comportement quand la personne visite la racine du site (http://web.tp2.linux/)
    location / {
        # on renvoie tout le trafic vers la machine web.tp2.linux
        proxy_pass http://web.tp2.linux;
    }
}
```

On check l'adresse avec un curl et on voit que la page a changé
```
PS C:\Users\kevin> curl 10.102.1.14
<!DOCTYPE html>
<html>
<head>
        <script> window.location.href="index.php"; </script>
        <meta http-equiv="refresh" content="0; URL=index.php">
</head>
</html>

```
## 3. Bonus HTTPS

# IV. Firewalling

## 1. Présentation de la syntaxe

## 2. Mise en place

### A. Base de données
Création de la zone db : 
```
[kraizix@db ~]$ sudo firewall-cmd --new-zone=db --permanent
[sudo] password for kraizix:
success
```

Config de la zone db :
```
[kraizix@db ~]$ sudo firewall-cmd --zone=db --add-source=10.102.1.11/24 --pe
rmanent
success
[kraizix@db ~]$ sudo firewall-cmd --zone=db --add-source=10.102.1.11/24
success
[kraizix@db ~]$ sudo firewall-cmd --zone=db --add-port=3306/tcp --permanent
success
[kraizix@db ~]$ sudo firewall-cmd --zone=db --add-port=3306/tcp
success
```

Vérification de la conf du firewall:
```
[kraizix@db ~]$ sudo firewall-cmd --get-active-zones
db
  sources: 10.102.1.11/24
drop
  interfaces: enp0s8 enp0s3
ssh
  sources: 10.102.1.1/24
[kraizix@db ~]$ sudo firewall-cmd --get-default-zone
drop
[kraizix@db ~]$ sudo firewall-cmd --list-all --zone=?
Error: INVALID_ZONE: ?
[kraizix@db ~]$ sudo firewall-cmd --list-all --zone=db
db (active)
  target: default
  icmp-block-inversion: no
  interfaces:
  sources: 10.102.1.11/24
  services:
  ports: 3306/tcp
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
[kraizix@db ~]$ sudo firewall-cmd --list-all --zone=ssh
ssh (active)
  target: default
  icmp-block-inversion: no
  interfaces:
  sources: 10.102.1.1/24
  services:
  ports: 22/tcp
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
[kraizix@db ~]$ sudo firewall-cmd --list-all --zone=drop
drop (active)
  target: DROP
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services:
  ports:
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
[kraizix@db ~]$

```
### B. Serveur Web
Ajout de la zone web:
```
[kraizix@web ~]$ sudo firewall-cmd --new-zone=web --permanent
success
[kraizix@web ~]$ sudo firewall-cmd --reload
success
[kraizix@web ~]$ sudo firewall-cmd --zone=web --add-source=10.102.1.14/24
success
[kraizix@web ~]$ sudo firewall-cmd --zone=web --add-source=10.102.1.14/24 --permanent
success
[kraizix@web ~]$ sudo firewall-cmd --zone=web --add-port=80/tcp
success
[kraizix@web ~]$ sudo firewall-cmd --zone=web --add-port=80/tcp --permanent
success

```

Vérif de la conf
```
[kraizix@web ~]$ sudo firewall-cmd --get-active-zones
drop
  interfaces: enp0s8 enp0s3
ssh
  sources: 10.102.1.1/24
web
  sources: 10.102.1.14/24
[kraizix@web ~]$ sudo firewall-cmd --get-default-zone
drop
[kraizix@web ~]$ sudo firewall-cmd --list-all --zone=web
web (active)
  target: default
  icmp-block-inversion: no
  interfaces:
  sources: 10.102.1.14/24
  services:
  ports: 80/tcp
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
[kraizix@web ~]$ sudo firewall-cmd --list-all --zone=ssh
ssh (active)
  target: default
  icmp-block-inversion: no
  interfaces:
  sources: 10.102.1.1/24
  services:
  ports: 22/tcp
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
[kraizix@web ~]$ sudo firewall-cmd --list-all --zone=drop
drop (active)
  target: DROP
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services:
  ports:
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
[kraizix@web ~]$

```
### C. Serveur de backup

```
[kraizix@backup backup]$ sudo firewall-cmd --new-zone=backup --permanent
success
[kraizix@backup backup]$ sudo firewall-cmd --reload
success
[kraizix@backup backup]$ sudo firewall-cmd --zone=backup --add-source=10.102
.1.11/24
success
[kraizix@backup backup]$ sudo firewall-cmd --zone=backup --add-source=10.102.1.11/24 --permanent
success
[kraizix@backup backup]$ sudo firewall-cmd --zone=backup --add-source=10.102.1.12/24
success
[kraizix@backup backup]$ sudo firewall-cmd --zone=backup --add-source=10.102.1.12/24 --permanent
success
[kraizix@backup backup]$ sudo firewall-cmd --zone=backup --add-port=19999/tcp
success
[kraizix@backup backup]$ sudo firewall-cmd --zone=backup --add-port=19999/tcp --permanent
success
[kraizix@backup backup]$ sudo firewall-cmd --zone=backup --add-port=2049/tcp
 --permanent
success
[kraizix@backup backup]$ sudo firewall-cmd --zone=backup --add-port=2049/tcp
success
[kraizix@backup backup]$
```

Vérification de la conf du firewall : 
```
[kraizix@backup backup]$ sudo firewall-cmd --get-active-zones
backup
  sources: 10.102.1.11/24 10.102.1.12/24
drop
  interfaces: enp0s8 enp0s3
ssh
  sources: 10.102.1.1/24
[kraizix@backup backup]$ sudo firewall-cmd --get-default-zone
drop
[kraizix@backup backup]$ sudo firewall-cmd --list-all --zone=backup
backup (active)
  target: default
  icmp-block-inversion: no
  interfaces:
  sources: 10.102.1.11/24 10.102.1.12/24
  services:
  ports: 19999/tcp 2049/tcp
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
[kraizix@backup backup]$ sudo firewall-cmd --list-all --zone=ssh
ssh (active)
  target: default
  icmp-block-inversion: no
  interfaces:
  sources: 10.102.1.1/24
  services:
  ports: 22/tcp
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
[kraizix@backup backup]$ sudo firewall-cmd --list-all --zone=drop
drop (active)
  target: DROP
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services:
  ports:
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
[kraizix@backup backup]$
```

### D. Reverse Proxy
Ajout de la zone proxy + restriction au réseau 10.102.1.0 :
```
[kraizix@front ~]$ sudo firewall-cmd --new-zone=proxy --permanent
success
[kraizix@front ~]$ sudo firewall-cmd --reload
success
[kraizix@front ~]$ sudo firewall-cmd --zone=proxy --add-source=10.102.1.0/24

success
[kraizix@front ~]$ sudo firewall-cmd --zone=proxy --add-source=10.102.1.0/24
 --permanent
success
[kraizix@front ~]$ sudo firewall-cmd --zone=proxy --add-port=80/tcp
success
[kraizix@front ~]$ sudo firewall-cmd --zone=proxy --add-port=80/tcp --perman
ent
success
```

Vérification du firewall :
```
[kraizix@front ~]$ sudo firewall-cmd --get-active-zones
drop
  interfaces: enp0s8 enp0s3
proxy
  sources: 10.102.1.0/24
ssh
  sources: 10.102.1.1/24
[kraizix@front ~]$ sudo firewall-cmd --get-default-zone
drop
[kraizix@front ~]$ sudo firewall-cmd --list-all --zone=proxy
proxy (active)
  target: default
  icmp-block-inversion: no
  interfaces:
  sources: 10.102.1.0/24
  services:
  ports:80/tcp
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
[kraizix@front ~]$ sudo firewall-cmd --list-all --zone=ssh
ssh (active)
  target: default
  icmp-block-inversion: no
  interfaces:
  sources: 10.102.1.1/24
  services:
  ports: 22/tcp
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
[kraizix@front ~]$ sudo firewall-cmd --list-all --zone=drop
drop (active)
  target: DROP
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services:
  ports:
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
[kraizix@front ~]$

```
### E. Tableau récap

| Machine            | IP            | Service                 | Port ouvert | IPs autorisées          |
| ------------------ | ------------- | ----------------------- | ----------- | ----------------------- |
| `web.tp2.linux`    | `10.102.1.11` | Serveur Web             | 80          | 10.102.1.14             |
| `db.tp2.linux`     | `10.102.1.12` | Serveur Base de Données | 3306        | 10.102.1.11             |
| `backup.tp2.linux` | `10.102.1.13` | Serveur de Backup (NFS) | 19999,2049  | 10.102.1.11,10.102.1.12 |
| `front.tp2.linux`  | `10.102.1.14` | Reverse Proxy           | 80          | réseau 10.102.1.0/24    |

